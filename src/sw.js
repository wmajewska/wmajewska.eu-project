// src/sw.js

// ESLint global registration
/* global serviceWorkerOption: false */

const cacheName = 'website-vue-cache';
const isExcluded = f => /hot-update|sockjs/.test(f);

const filesToCache = [
  ...serviceWorkerOption.assets.filter(file => !isExcluded(file)),
  '/',
  'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'
];


const updateCache = (request, response) =>
  caches.open(cacheName).then(cache => cache.put(request, response));


const fetchFromNetwork = request =>
  new Promise((resolve, reject) => {
    fetch(request).then(response => {
      if (!isExcluded(request.url) && response) {
        updateCache(request, response.clone()).then(() => resolve(response));
      } else {
        resolve(response);
      }
    }, reject);
  });


  const fetchFromCache = request =>
    caches.match(request).then(response => response || Promise.reject('failed'));


// Cache known assets up-front
const preCache = () =>
  caches.open(cacheName).then(cache => {
    cache.addAll(filesToCache);
  });

// Handle the 'install' event
self.addEventListener('install', event => {
  event.waitUntil(preCache());
});

self.addEventListener('fetch', event => {
  event.respondWith(
    fetchFromNetwork(event.request).catch(() => fetchFromCache(event.request))
  );
});

// INSTALL BANNER
self.addEventListener('beforeinstallprompt', (e) => {
  // Prevent Chrome 67 and earlier from automatically showing the prompt
  e.preventDefault();
  // Stash the event so it can be triggered later.
  deferredPrompt = e;
  // Update UI notify the user they can add to home screen
  btnAdd.style.display = 'block';

});

// btnAdd.addEventListener('click', (e) => {
//   // hide our user interface that shows our A2HS button
//   btnAdd.style.display = 'none';
//   // Show the prompt
//   deferredPrompt.prompt();
//   // Wait for the user to respond to the prompt
//   deferredPrompt.userChoice
//     .then((choiceResult) => {
//       if (choiceResult.outcome === 'accepted') {
//         console.log('User accepted the A2HS prompt');
//       } else {
//         console.log('User dismissed the A2HS prompt');
//       }
//       deferredPrompt = null;
//     });
// });
